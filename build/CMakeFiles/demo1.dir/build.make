# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.10

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/danny/project/demo1/maketest

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/danny/project/demo1/maketest/build

# Include any dependencies generated for this target.
include CMakeFiles/demo1.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/demo1.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/demo1.dir/flags.make

CMakeFiles/demo1.dir/src/hellomake.c.o: CMakeFiles/demo1.dir/flags.make
CMakeFiles/demo1.dir/src/hellomake.c.o: ../src/hellomake.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/danny/project/demo1/maketest/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building C object CMakeFiles/demo1.dir/src/hellomake.c.o"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/demo1.dir/src/hellomake.c.o   -c /home/danny/project/demo1/maketest/src/hellomake.c

CMakeFiles/demo1.dir/src/hellomake.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/demo1.dir/src/hellomake.c.i"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/danny/project/demo1/maketest/src/hellomake.c > CMakeFiles/demo1.dir/src/hellomake.c.i

CMakeFiles/demo1.dir/src/hellomake.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/demo1.dir/src/hellomake.c.s"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/danny/project/demo1/maketest/src/hellomake.c -o CMakeFiles/demo1.dir/src/hellomake.c.s

CMakeFiles/demo1.dir/src/hellomake.c.o.requires:

.PHONY : CMakeFiles/demo1.dir/src/hellomake.c.o.requires

CMakeFiles/demo1.dir/src/hellomake.c.o.provides: CMakeFiles/demo1.dir/src/hellomake.c.o.requires
	$(MAKE) -f CMakeFiles/demo1.dir/build.make CMakeFiles/demo1.dir/src/hellomake.c.o.provides.build
.PHONY : CMakeFiles/demo1.dir/src/hellomake.c.o.provides

CMakeFiles/demo1.dir/src/hellomake.c.o.provides.build: CMakeFiles/demo1.dir/src/hellomake.c.o


CMakeFiles/demo1.dir/src/main.c.o: CMakeFiles/demo1.dir/flags.make
CMakeFiles/demo1.dir/src/main.c.o: ../src/main.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/danny/project/demo1/maketest/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building C object CMakeFiles/demo1.dir/src/main.c.o"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/demo1.dir/src/main.c.o   -c /home/danny/project/demo1/maketest/src/main.c

CMakeFiles/demo1.dir/src/main.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/demo1.dir/src/main.c.i"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/danny/project/demo1/maketest/src/main.c > CMakeFiles/demo1.dir/src/main.c.i

CMakeFiles/demo1.dir/src/main.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/demo1.dir/src/main.c.s"
	/usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/danny/project/demo1/maketest/src/main.c -o CMakeFiles/demo1.dir/src/main.c.s

CMakeFiles/demo1.dir/src/main.c.o.requires:

.PHONY : CMakeFiles/demo1.dir/src/main.c.o.requires

CMakeFiles/demo1.dir/src/main.c.o.provides: CMakeFiles/demo1.dir/src/main.c.o.requires
	$(MAKE) -f CMakeFiles/demo1.dir/build.make CMakeFiles/demo1.dir/src/main.c.o.provides.build
.PHONY : CMakeFiles/demo1.dir/src/main.c.o.provides

CMakeFiles/demo1.dir/src/main.c.o.provides.build: CMakeFiles/demo1.dir/src/main.c.o


# Object files for target demo1
demo1_OBJECTS = \
"CMakeFiles/demo1.dir/src/hellomake.c.o" \
"CMakeFiles/demo1.dir/src/main.c.o"

# External object files for target demo1
demo1_EXTERNAL_OBJECTS =

demo1: CMakeFiles/demo1.dir/src/hellomake.c.o
demo1: CMakeFiles/demo1.dir/src/main.c.o
demo1: CMakeFiles/demo1.dir/build.make
demo1: CMakeFiles/demo1.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/danny/project/demo1/maketest/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Linking C executable demo1"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/demo1.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/demo1.dir/build: demo1

.PHONY : CMakeFiles/demo1.dir/build

CMakeFiles/demo1.dir/requires: CMakeFiles/demo1.dir/src/hellomake.c.o.requires
CMakeFiles/demo1.dir/requires: CMakeFiles/demo1.dir/src/main.c.o.requires

.PHONY : CMakeFiles/demo1.dir/requires

CMakeFiles/demo1.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/demo1.dir/cmake_clean.cmake
.PHONY : CMakeFiles/demo1.dir/clean

CMakeFiles/demo1.dir/depend:
	cd /home/danny/project/demo1/maketest/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/danny/project/demo1/maketest /home/danny/project/demo1/maketest /home/danny/project/demo1/maketest/build /home/danny/project/demo1/maketest/build /home/danny/project/demo1/maketest/build/CMakeFiles/demo1.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/demo1.dir/depend

